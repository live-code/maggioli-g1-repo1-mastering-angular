import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <h1 [style.color]="color">Hello {{name}}</h1>
  `
})
export class HelloComponent {
  @Input() name = 'Guest';
  @Input() color = 'red';
}
