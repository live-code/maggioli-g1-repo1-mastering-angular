import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HelloComponent } from './components/hello.component';
import { HomeComponent } from './features/home/home.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { CrudComponent } from './features/crud/crud.component';
import { AppTvmazeComponent } from './features/tvmaze/app-tvmaze.component';
import { RouterModule } from '@angular/router';
import { UserDetailsComponent } from './features/user-details/user-details.component';
import { NavbarComponent } from './core/navbar.component';
import { SettingsComponent } from './features/settings/settings.component';
import { TvmazeService } from './features/tvmaze/services/tvmaze.service';
import { IconComponent } from './shared/icon.component';
import { CrudUserDataComponent } from './features/crud/components/crud-user-data.component';
import { GmapComponent } from './shared/gmap.component';
import { CrudUserListComponent } from './features/crud/components/crud-user-list.component';

@NgModule({
  declarations: [
    AppComponent, HelloComponent, HomeComponent, ContactsComponent, CrudComponent,
    AppTvmazeComponent,
    UserDetailsComponent,
    NavbarComponent,
    SettingsComponent,
    IconComponent,
    CrudUserDataComponent,
    GmapComponent,
    CrudUserListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'users', component: CrudComponent },
      { path: 'users/:id', component: UserDetailsComponent },
      { path: 'tvmaze', component: AppTvmazeComponent },
      { path: 'contacts', component: ContactsComponent },
      { path: 'settings', component: SettingsComponent },
      { path: '', component: HomeComponent },
      { path: '**', redirectTo: '' },
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
