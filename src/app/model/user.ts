export interface User {
  id: number;
  name: string;
  age: number;
  gender: 'M' | 'F';
  city?: string;
  birthday: number;
  bitcoins: number;
  address?: Address;
}

// ALIAS TYPE

export type PartialUser = Pick<User, 'name' | 'age'>

export interface Address {
  lat: number;
  lng: number;
}

