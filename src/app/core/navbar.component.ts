import { Component, OnInit } from '@angular/core';
import { ThemeService } from './theme.service';

@Component({
  selector: 'app-navbar',
  template: `

    <div 
      style="padding: 20px; border-radius: 10px"
      [style.backgroundColor]="themeService.value === 'light' ? '#ccc' : '#000'"
    >
      <button routerLink="tvmaze">tvmaze</button>
      <button routerLink="contacts">contacts</button>
      <button routerLink="users">users</button>
      <button routerLink="settings">settings</button>
      <button routerLink="">home</button>
    </div>
    {{themeService.value}}
  `,
})
export class NavbarComponent {

  constructor(public themeService: ThemeService) {
    console.log(themeService.value)
  }
}
