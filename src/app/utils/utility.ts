import { compareNumbers } from '@angular/compiler-cli/src/diagnostics/typescript_version';

export function add(a, b): number {
  return a + b;
}

export function divide(a, b): number {
  return a / b;
}
