import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gmap',
  template: `
    <div>
      <img *ngIf="city"
           [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + city + '&zoom=5&size=200x50&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
    </div>
  `,
})
export class GmapComponent  {
  @Input() city: string;

}
