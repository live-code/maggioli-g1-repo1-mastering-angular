import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-icon',
  template: `
    <i
      class="fa"
      [ngClass]="{
        'fa-venus': value === 'F',
        'fa-mars': value === 'M'
      }"
    ></i>
  `,
})
export class IconComponent {
  @Input() value: string;
}
