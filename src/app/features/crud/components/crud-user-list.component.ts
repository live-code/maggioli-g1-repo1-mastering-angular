import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-crud-user-list',
  template: `
    <li
      *ngFor="let user of users" class="list-group-item"
      [ngClass]="{'active': user.id === active.id}"
      (click)="setActive.emit(user)"
    >
<!--    [routerLink]="'/users/' + user.id"-->

      <app-crud-user-data [user]="user"></app-crud-user-data>
      <i class="fa fa-trash pull-right" 
         (click)="deleteUserHandler(user.id, $event)"></i>
      <app-gmap [city]="user.city"></app-gmap>
    </li>
  `,
})
export class CrudUserListComponent {
  @Input() users: User[];
  @Input() active: User;
  @Output() deleteUser: EventEmitter<number> = new EventEmitter<number>();
  @Output() setActive: EventEmitter<User> = new EventEmitter<User>();

  deleteUserHandler(id: number, event: MouseEvent): void {
    event.stopPropagation();
    this.deleteUser.emit(id);
  }
}
