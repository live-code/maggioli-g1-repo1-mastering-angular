import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-crud-user-data',
  template: `
    <span *ngIf="user.name">
      {{user.name}}
      {{user.gender}}
       <app-icon [value]="user.gender"></app-icon>
    </span>
  `,
})
export class CrudUserDataComponent {
  @Input() user: User;
}
