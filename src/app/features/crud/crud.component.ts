import { Component } from '@angular/core';
import {  User } from '../../model/user';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-crud',
  template: `
    <form #f="ngForm" (submit)="save(f)" style="padding: 20px">
      
      <small *ngIf="nameInput.errors?.required">Campo obbligatorio</small>
      <small *ngIf="nameInput.errors?.minlength">Minimo 3 caratteri</small>
      <br>
    
      <input 
        type="text"
        #nameInput="ngModel" 
        name="name" 
        placeholder="name" 
        [ngModel]="active.name"
        required 
        minlength="3"
        class="form-control"
        [ngClass]="{'is-invalid': nameInput.invalid && f.dirty}"
      >
      
      <input 
        type="text" name="age" placeholder="age" #ageInput="ngModel" 
        [ngModel]="active.age" required class="form-control"
        [ngClass]="{'is-invalid': ageInput.invalid && f.dirty}"
      >
      
      <select class="form-control" name="gender" [ngModel]="active.gender">
        <option [ngValue]="null">Select gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>    
      </select>
      <button type="submit" [disabled]="f.invalid">
        {{active.id ? 'EDIT' : 'ADD'}}
      </button>
      
      <button type="button" (click)="resetForm(f)">CLEAN</button>
    </form>
    
    <div class="container mt-5">
      <app-crud-user-list 
        [users]="users" 
        [active]="active"
        (deleteUser)="deleteUser($event)"
        (setActive)="setActive($event)"
      ></app-crud-user-list>
      
    </div>
    
  `,
})
export class CrudComponent {
  active: User = { gender: null } as User;
  users: User[];

  constructor(private http: HttpClient) {
    this.getUsers();
  }

  getUsers(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe((result) => {
        this.users = result;
      });
  }

  save(form: NgForm): void {
    if (this.active.id) {
      this.editUser(form);
    } else {
      this.addUser(form);
    }

  }

  addUser(form: NgForm): void {
    this.http.post<User>('http://localhost:3000/users', form.value)
      .subscribe((result) => {
        this.users.push(result);
        this.active = result;
        form.reset();
      });

  }

  editUser(form: NgForm): void {
    this.http.patch<User>('http://localhost:3000/users/' + this.active.id, form.value)
      .subscribe(res => {
        const index = this.users.findIndex(user => user.id === this.active.id);
        // this.users[index] = Object.assign({}, this.active, form.value);
        this.users[index] = res;
      });

  }

  deleteUser(id: number): void {
    this.http.delete('http://localhost:3000/users/' + id)
      .subscribe(() => {
        const index = this.users.findIndex(user => user.id === id);
        this.users.splice(index, 1);
        if (this.active.id === id) {
          this.active = { gender: null } as User;
        }
      });
  }

  setActive(user: User): void {
    this.active = user;
  }

  resetForm(form: NgForm): void {
    form.reset();
    this.active = { gender: null } as User;
  }
}
