import { Component } from '@angular/core';
import { TvmazeService } from './services/tvmaze.service';

@Component({
  selector: 'app-tvmaze',
  template: `
    <form #f="ngForm" (submit)="tvmazeService.submit(f.value.text)">
      <input type="text" [ngModel] name="text" required>
      <button type="submit" [disabled]="f.invalid" hidden>GO</button>
    </form>

    <div class="grid">
      <div
        *ngFor="let series of tvmazeService.result" class="grid-item"
        (click)="tvmazeService.openModal(series.show)"
      >
        <div class="movie">
          <img *ngIf="series.show.image" [src]="series.show.image?.medium" alt="">
          <div class="noImage" *ngIf="!series.show.image">no image</div>
          <div class="movieText">{{series.show.name}}</div>
        </div>
      </div>
    </div>

    <div class="wrapper" *ngIf="tvmazeService.show">
      <div class="content">
        <h3>{{tvmazeService.show?.name}}</h3>
        <img [src]="tvmazeService.show.image?.original" width="100%">
        <div class="tag" *ngFor="let genres of tvmazeService.show?.genres">
          {{genres}}
        </div>
        <p [innerHTML]="tvmazeService.show?.summary">....</p>
        <a class="button" [href]="tvmazeService.show?.url" target="_blank" rel="noopener noreferrer">Visit website</a>
      </div>

      <div class="closeButton" (click)="tvmazeService.closeModal()">×</div>
    </div>
  `,
  providers: [TvmazeService],
  styleUrls: ['tvmaze.component.css', './tvmaze-modal.component.css']
})
export class AppTvmazeComponent {
  constructor(public tvmazeService: TvmazeService) {
    this.tvmazeService.submit('friends')
  }
}
