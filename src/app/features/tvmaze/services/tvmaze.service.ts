import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Series, Show } from '../../../model/series';
import { Observable } from 'rxjs';

@Injectable()
export class TvmazeService {
  result: Series[];
  show: Show;

  constructor(private http: HttpClient) {}

  submit(text: string): void {
    this.http.get<Series[]>(`http://api.tvmaze.com/search/shows?q=${text}`)
      .subscribe(result => {
        this.result = result;
      });
  }

  openModal(show: Show): void {
    this.show = show;
  }

  closeModal(): void {
    this.show = null;
  }
}
