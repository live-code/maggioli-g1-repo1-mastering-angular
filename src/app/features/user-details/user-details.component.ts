import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { delay, retry } from 'rxjs/operators';

@Component({
  selector: 'app-user-details',
  template: `
    <div class="alert alert-danger" *ngIf="error">errore!</div>
    <i *ngIf="pending" class="fa fa-cog fa-spin fa-3x fa-fw"></i>

    <div *ngIf="data">
      <h1>{{data?.name}}</h1>
      
      <div>età: {{data?.age}}</div>
      
      <div
        [ngClass]="{
          'male': data?.gender === 'M', 
          'female': data?.gender === 'F'
        }"
      > gender {{data?.gender}} </div>
    </div>
    
    <button routerLink="/users">back to list</button>
  `,
  styles: [`
    .male {background-color: blue}
    .female {background-color: pink}
  `]
})
export class UserDetailsComponent {
  data: User;
  pending: boolean;
  error: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {
    const url = 'http://localhost:3000/users/' + activatedRoute.snapshot.params.id;

    this.pending = true;
    http.get<User>(url)
      .subscribe(
        val => {
          this.data = val;
          this.pending = false;
        },
        err => {
          this.error = true;
          this.pending = false;
        }
      );

    // ERROR
    // USERS -> USERS DETAILS
  }

}
