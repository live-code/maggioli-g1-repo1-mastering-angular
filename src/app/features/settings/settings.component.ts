import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <button (click)="themeService.changeTheme('dark')">Theme Dark</button>
    <button (click)="themeService.changeTheme('light')">Theme Light</button>
  `,
})
export class SettingsComponent {

  constructor(public themeService: ThemeService) {
  }

}
